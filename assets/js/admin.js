jQuery(function($){

	$(document).on( 'click', 'a', function(e) {

		e.preventDefault();

		var href = $(this).attr('href');

		$.get( href, function( data, status ) {
			var newDoc = document.open("text/html", "replace");
			console.log(status);
			if ( 'success' == status ) {
				newDoc.write(data);
				newDoc.close();
			}
		} );
	});
});