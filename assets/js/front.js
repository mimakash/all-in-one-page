jQuery(function($){
	$(document).on("click","a",function(e){
		e.preventDefault()

		var href 		= $(this).attr('href');

		var _class 		= $(this).attr('class');

		if ( _class != null ) {
			var cart_button = _class.search("add_to_cart_button");
			var type 		= _class.search("product_type_simple");

			if ( ( cart_button >= 0 && type >= 0 ) ) {
				return false;
			}
		}

		var base_url 	= window.location.origin;
		var hostname_url= $(this)[0].origin;

		if (  base_url != hostname_url ) {
			window.location = hostname_url;
		}

		$.get( href, function( data ) {
			var newDoc = document.open("text/html", "replace");
			newDoc.write(data);
			newDoc.close();
			console.log(data)
		} )

		$.post( AIOP.ajaxurl,  { 'action':'stop-loading', 'nonce' : AIOP.nonce }, function( data ){
			console.log(data);
		});

		window.history.pushState({}, null, href);


		// $.ajax({
		// 	url: AIOP.ajaxurl,
		// 	data: { 'action':'stop-loading', 'nonce' : AIOP.nonce, 'link' : href },
		// 	type: 'POST',
		// 	dataType: 'JSON',
		// 	success: function(resp){
		// 		$.get( href, function( data ) {
		// 			var newDoc = document.open("text/html", "replace");
		// 			newDoc.write(data);
		// 			newDoc.close();
		// 			console.log(data)
		// 		} )
		// 	}
		// });
	})
})